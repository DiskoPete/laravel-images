<?php

namespace DiskoPete\LaravelImages\Contracts\Image\Query;


use DiskoPete\LaravelImages\Models\Image\Query;

interface Factory
{
    public function make(): Query;
}
