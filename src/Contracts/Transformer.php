<?php

namespace DiskoPete\LaravelImages\Contracts;


interface Transformer
{
    public function getUrl(): string;

    public function fit(int $width, int $height): Transformer;

    public function resize(int $width = null, int $height = null): Transformer;

    public function getNaturalDimensions(): Dimensions;
}
