<?php

namespace DiskoPete\LaravelImages\Contracts;


interface Image
{
    public function getPath(): string;

    public function getAbsolutePath(): string;

    public function getUrl(): string;
}
