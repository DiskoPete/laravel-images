<?php


namespace DiskoPete\LaravelImages\Contracts;


interface Dimensions
{
    public function setWidth(int $width): Dimensions;

    public function getWidth(): int;

    public function setHeight(int $height): Dimensions;

    public function getHeight(): int;
}
