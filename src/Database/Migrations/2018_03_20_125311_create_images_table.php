<?php

use DiskoPete\LaravelImages\Models\Image;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Image::TABLE_NAME, function (Blueprint $table) {
            $table->increments('id');
            $table->string(Image::COLUMN_SUBJECT_TYPE);
            $table->unsignedInteger(Image::COLUMN_SUBJECT_ID);
            $table->string(Image::COLUMN_PATH);
            $table->integer(Image::COLUMN_POSITION)->nullable();
            $table->string(Image::COLUMN_TYPE)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Image::TABLE_NAME);
    }
}
