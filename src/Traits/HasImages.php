<?php

namespace DiskoPete\LaravelImages\Traits;


use DiskoPete\LaravelImages\Models\Image;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\MorphMany;

/**
 * @property-read Collection images
 */
trait HasImages
{
    public function images(): MorphMany
    {
        return $this->morphMany(Image::class, 'subject');
    }

    /**
     * Applies the default order to the images attribute
     *
     * @return Collection
     */
    public function getImagesAttribute(): Collection
    {
        return $this->images()
            ->orderBy(Image::COLUMN_POSITION)
            ->get();
    }
}
