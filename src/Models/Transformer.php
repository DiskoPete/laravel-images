<?php

namespace DiskoPete\LaravelImages\Models;


use DiskoPete\LaravelImages\Contracts\Dimensions;
use DiskoPete\LaravelImages\Contracts\Image as ImageContract;
use DiskoPete\LaravelImages\Contracts\Transformer as TransformerContract;
use Illuminate\Contracts\Container\Container;
use Illuminate\Filesystem\FilesystemAdapter;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Constraint;
use Intervention\Image\ImageManager;

class Transformer implements TransformerContract
{
    const KEY_METHOD    = 'method';
    const KEY_ARGUMENTS = 'arguments';
    const KEY_CALLBACK  = 'callback';

    /**
     * @var ImageContract
     */
    private $image;
    /**
     * @var ImageManager
     */
    private $imageManager;

    private $transformations = [];
    /**
     * @var Container
     */
    private $container;

    public function __construct(
        ImageContract $image,
        ImageManager $imageManager,
        Container $container
    )
    {
        $this->image        = $image;
        $this->imageManager = $imageManager;
        $this->container    = $container;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {

        if (!$this->hasTransformations()) {
            return $this->image->getUrl();
        }

        $destination = $this->composeDestination();
        $filesystem  = $this->getFilesystem();

        $this->generateImage($destination);

        return $filesystem->url($destination);
    }

    private function generateImage(string $destination): void
    {
        $filesystem = $this->getFilesystem();

        if ($filesystem->exists($destination)) {
            return;
        }

        $image = $this->imageManager->make($this->image->getAbsolutePath());
        $this->applyTransformations($image);
        $this->saveImage($image, $destination);
    }

    private function getFilesystem(): FilesystemAdapter
    {
        return Storage::disk('public');
    }

    private function hasTransformations(): bool
    {
        return !empty($this->transformations);
    }

    /**
     * @return string
     */
    private function composeDestination(): string
    {
        $pathFragments = [
            'cached',
            dirname($this->image->getPath()),
            pathinfo($this->image->getPath(), PATHINFO_FILENAME),
            $this->generateBasename()
        ];

        return implode(DIRECTORY_SEPARATOR, $pathFragments);
    }

    private function generateBasename(): string
    {
        $holder    = [];
        $extension = pathinfo($this->image->getPath(), PATHINFO_EXTENSION);

        foreach ($this->transformations as $transformation) {

            $holder[] = $transformation[self::KEY_METHOD];

            foreach ($transformation[self::KEY_ARGUMENTS] as $value) {

                if ($value instanceof \Closure) {
                    continue;
                }

                $holder[] = $value;
            }

        }

        return md5(implode('|', $holder)) . '.' . $extension;
    }

    /**
     * Method set protected to test cacheability
     *
     * @param \Intervention\Image\Image $img
     */
    protected function applyTransformations(\Intervention\Image\Image $img): void
    {
        foreach ($this->transformations as $transformation) {
            call_user_func_array([$img, $transformation[self::KEY_METHOD]], $transformation[self::KEY_ARGUMENTS]);
        }
    }

    private function saveImage(\Intervention\Image\Image $image, string $destination): void
    {
        $filesystem = $this->getFilesystem();
        $filesystem->makeDirectory(dirname($destination));
        $image->save($filesystem->path($destination));
    }

    public function fit(int $width, int $height): TransformerContract
    {
        $this->transformations[] = [
            self::KEY_METHOD    => 'fit',
            self::KEY_ARGUMENTS => [
                $width,
                $height,
                function (Constraint $constraint) {
                    $constraint->upsize();
                }
            ]
        ];

        return $this;
    }

    public function resize(int $width = null, int $height = null): TransformerContract
    {
        $this->transformations[] = [
            self::KEY_METHOD    => 'resize',
            self::KEY_ARGUMENTS => [
                $width,
                $height,
                function (Constraint $constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                }
            ]
        ];

        return $this;
    }

    public function getNaturalDimensions(): Dimensions
    {
        $destination = $this->composeDestination();

        $this->generateImage($destination);

        $filesystem = $this->getFilesystem();

        list($width, $height) = getimagesize($filesystem->path($destination));

        $dimensions = $this->makeDimensions();
        $dimensions->setWidth($width);
        $dimensions->setHeight($height);

        return $dimensions;
    }

    private function makeDimensions(): Dimensions
    {
        return $this->container->make(Dimensions::class);
    }
}
