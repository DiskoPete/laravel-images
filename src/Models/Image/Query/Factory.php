<?php

namespace DiskoPete\LaravelImages\Models\Image\Query;


use DiskoPete\LaravelImages\Contracts\Image\Query\Factory as FactoryContract;
use DiskoPete\LaravelImages\Models\Image;
use DiskoPete\LaravelImages\Models\Image\Query;

class Factory implements FactoryContract
{

    /**
     * @var Image
     */
    protected $imageInstance;

    public function __construct(Image $model)
    {
        $this->imageInstance = $model;
    }

    public function make(): Query
    {
        return $this->imageInstance->newQueryWithoutScopes();
    }
}
