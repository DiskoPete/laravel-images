<?php

namespace DiskoPete\LaravelImages\Models\Image;


use DiskoPete\LaravelImages\Models\Image;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Query extends Builder
{
    /**
     * @deprecated Use addSubjectFilter instead
     * @param Model $model
     * @return Query
     */
    public function addModelFilter(Model $model): Query
    {
        return $this->addSubjectFilter($model);
    }

    public function addSubjectFilter(Model $subject): Query
    {
        return $this
            ->where(Image::COLUMN_SUBJECT_TYPE, get_class($subject))
            ->where(Image::COLUMN_SUBJECT_ID, $subject->getKey());
    }

    public function addTypeFilter(string $type): Query
    {
        return $this->where(Image::COLUMN_TYPE, $type);
    }
}
