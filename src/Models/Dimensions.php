<?php


namespace DiskoPete\LaravelImages\Models;


class Dimensions implements \DiskoPete\LaravelImages\Contracts\Dimensions
{

    /**
     * @var int
     */
    private $width;
    /**
     * @var int
     */
    private $height;

    public function getWidth(): int
    {
        return $this->width;
    }

    public function setWidth(int $width): \DiskoPete\LaravelImages\Contracts\Dimensions
    {
        $this->width = $width;

        return $this;
    }

    public function getHeight(): int
    {
        return $this->height;
    }

    public function setHeight(int $height): \DiskoPete\LaravelImages\Contracts\Dimensions
    {
        $this->height = $height;

        return $this;
    }
}