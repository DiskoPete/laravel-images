<?php

namespace DiskoPete\LaravelImages\Models;

use DiskoPete\LaravelImages\Contracts\Image as ImageContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Filesystem\FilesystemAdapter;
use Illuminate\Support\Facades\Storage;
use DiskoPete\LaravelImages\Models\Image\Query;

/**
 * @property string path Path on filesystem
 * @property int id
 * @property int position
 * @property Model subject
 */
class Image extends Model implements ImageContract
{

    public const COLUMN_PATH         = 'path';
    public const COLUMN_POSITION     = 'position';
    public const COLUMN_SUBJECT_ID   = 'subject_id';
    public const COLUMN_SUBJECT_TYPE = 'subject_type';
    public const COLUMN_TYPE         = 'type';

    public const TABLE_NAME = 'images';

    protected $table = self::TABLE_NAME;

    protected $fillable = [
        self::COLUMN_PATH,
        self::COLUMN_POSITION,
    ];

    /**
     * Get url to image
     *
     * @return string
     */
    public function getUrl(): string
    {
        $filesystem = $this->getFilesystem();

        return $filesystem->url($this->path);
    }

    /**
     * @return \Illuminate\Contracts\Filesystem\Filesystem
     */
    private function getFilesystem(): FilesystemAdapter
    {
        return Storage::disk('public');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function subject(): MorphTo
    {
        return $this->morphTo();
    }

    public function getAbsolutePath(): string
    {
        return $this->getFilesystem()->path($this->path);
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function newEloquentBuilder($query): Query
    {
        return new Query($query);
    }
}
