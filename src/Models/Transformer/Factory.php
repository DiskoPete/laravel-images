<?php

namespace DiskoPete\LaravelImages\Models\Transformer;


use DiskoPete\LaravelImages\Contracts\Image;
use DiskoPete\LaravelImages\Models\Transformer;
use Illuminate\Contracts\Container\Container;

class Factory
{
    /**
     * @var Container
     */
    private $container;

    public function __construct(
        Container $container
    )
    {
        $this->container = $container;
    }

    public function make(Image $image): Transformer
    {
        return $this->container->make(
            Transformer::class,
            [
                'image' => $image
            ]
        );
    }
}
