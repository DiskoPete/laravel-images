<?php

namespace DiskoPete\LaravelImages\Providers;

use DiskoPete\LaravelImages\Contracts\Dimensions as DimensionsContract;
use DiskoPete\LaravelImages\Contracts\Image\Query\Factory as ImageQueryFactoryContract;
use DiskoPete\LaravelImages\Models\Dimensions;
use DiskoPete\LaravelImages\Models\Image\Query\Factory;
use DiskoPete\LaravelImages\Models\Transformer\Factory as TransformerFactory;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use Intervention\Image\ImageManager;

class ServiceProvider extends BaseServiceProvider
{

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerConfig();
        $this->loadMigrations();
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__ . '/../Config/config.php' => config_path('images.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__ . '/../Config/config.php', 'images'
        );
    }


    private function loadMigrations()
    {
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    public function register(): void
    {
        $this->app->singleton(ImageManager::class);
        $this->app->singleton(TransformerFactory::class);

        $this->app->singleton(
            ImageQueryFactoryContract::class,
            Factory::class
        );

        $this->app->bind(
            DimensionsContract::class,
            Dimensions::class
        );
    }
}
