<?php

use Illuminate\Support\Facades\Route;

Route::post(
    config('images.upload.route'),
    'UploadController@execute'
)->name('image.upload');
