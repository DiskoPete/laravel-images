<?php

namespace DiskoPete\LaravelImages\Http\Controllers;


use Illuminate\Contracts\Filesystem\Factory;
use Illuminate\Filesystem\FilesystemAdapter;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Intervention\Image\Constraint;
use Intervention\Image\Image;
use Intervention\Image\ImageManager;


class UploadController extends Controller
{

    /**
     * @var Request
     */
    private $request;
    /**
     * @var Str
     */
    private $strHelper;
    /**
     * @var Factory
     */
    private $filesystemFactory;
    /**
     * @var ImageManager
     */
    private $imageManager;

    public function __construct(
        Request $request,
        Str $strHelper,
        Factory $filesystemFactory,
        ImageManager $imageManager
    )
    {
        $this->request           = $request;
        $this->strHelper         = $strHelper;
        $this->filesystemFactory = $filesystemFactory;
        $this->imageManager      = $imageManager;
    }

    public function execute()
    {
        // validate request
        $this->request->validate(
            [
                'image' => 'required|image',
            ]
        );

        /** @var FilesystemAdapter $filesystem */
        $filesystem  = $this->getFilesystem();
        $file        = $this->request->file('image');
        $path        = 'tmp';
        $name        = $file->hashName();
        $destination = $path . '/' . $file->hashName();
        $count       = 0;

        // Generate destination string
        while ($filesystem->exists($destination) && $count < 100) {

            $name        = $this->strHelper->random(40) . '.' . $file->getClientOriginalExtension();
            $destination = $path . DIRECTORY_SEPARATOR . $name;
            $count++;
        }

        $result = $filesystem->putFileAs(
            $path,
            $file,
            $name
        );

        $url = $this->generateUrl($result);

        return [
            'path' => $result,
            'url'  => $url,
        ];
    }

    private function getFilesystem(): FilesystemAdapter
    {
        return $this->filesystemFactory->disk('public');
    }

    private function generateUrl(?string $path): ?string
    {
        if (!$path) {
            return null;
        }

        $preview = $this->getPreviewOptions();

        $fitWidth  = $preview['fit']['width'] ?? null;
        $fitHeight = $preview['fit']['height'] ?? null;

        if ($fitWidth && $fitHeight) {
            $path = $this->fitImage($path, $fitWidth, $fitHeight);
        }

        $resizeWidth  = $preview['resize']['width'] ?? null;
        $resizeHeight = $preview['resize']['height'] ?? null;

        if ($resizeWidth || $resizeHeight) {
            $path = $this->resizeImage($path, $resizeWidth, $resizeHeight);
        }

        return $this->getFilesystem()->url($path);

    }

    private function getPreviewOptions(): ?array
    {
        $preview = $this->request->input('preview');

        if (!$preview) {
            return null;
        }

        if (is_string($preview)) {
            $preview = json_decode($preview, true);
        }

        return $preview;
    }

    private function fitImage(string $path, int $width, int $height): string
    {
        $image = $this->makeImage($path);

        $image->fit($width, $height, function (Constraint $constraint) {
            $constraint->upsize();
        });

        $destination = $this->generateDestinationPath($path);

        $image->save(
            $this->getFilesystem()->path($destination)
        );

        return $destination;
    }

    private function makeImage(string $path): Image
    {
        $image = $this->imageManager->make(
            $this->getFilesystem()->path($path)
        );
        return $image;
    }

    /**
     * @param string $path
     * @return string
     */
    private function generateDestinationPath(string $path): string
    {
        $extension = pathinfo($path, PATHINFO_EXTENSION);

        $pathFragments = [
            dirname($path),
            pathinfo($path, PATHINFO_FILENAME) . '-' . uniqid() . '.' . $extension,
        ];

        $destination = implode(DIRECTORY_SEPARATOR, $pathFragments);
        return $destination;
    }

    private function resizeImage(string $path, int $width, int $height = null): string
    {
        $image = $this->makeImage($path);

        $image->resize($width, $height, function (Constraint $constraint) {
            $constraint->upsize();
            $constraint->aspectRatio();
        });

        $destination = $this->generateDestinationPath($path);

        $image->save(
            $this->getFilesystem()->path($destination)
        );

        return $destination;
    }
}
