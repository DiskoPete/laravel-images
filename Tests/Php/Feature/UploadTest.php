<?php

namespace DiskoPete\LaravelImages\Tests\Php\Feature;


use DiskoPete\LaravelImages\Tests\Php\TestCase;
use Illuminate\Contracts\Filesystem\Factory;
use Illuminate\Filesystem\FilesystemAdapter;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Http\Testing\FileFactory;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;

class UploadTest extends TestCase
{

    /**
     * @var FileFactory
     */
    private $fileFactory;

    /**
     * @var Factory
     */
    private $fileSystem;


    /**
     * @test
     */
    public function mustProvideAValidImage()
    {
        $this->expectException(ValidationException::class);

        $this
            ->postJson(
                route('image.upload'),
                [
                    'image' => 'not-an-image',
                ]
            );
    }

    /**
     * @test
     */
    public function storesFileToPublicDisk()
    {

        $file     = $this->fileFactory->image('some-image.jpg');
        $response = $this->postJson(
            route('image.upload'),
            [
                'image' => $file,
            ]
        );

        $response->assertStatus(200);

        $this->assertImageExists($response);

        $this->assertArrayHasKey('url', $response->json());
    }

    private function assertImageExists(TestResponse $response): void
    {
        /** @var FilesystemAdapter $filesystem */
        $filesystem = $this->fileSystem->disk('public');
        $filesystem->assertExists(
            $response->json('path')
        );
    }

    /**
     * @test
     */
    public function canUploadSameImageName()
    {
        /** @var FilesystemAdapter $filesystem */
        $filesystem = $this->fileSystem->disk('public');

        $this->postJson(
            route('image.upload'),
            [
                'image' => $this->fileFactory->image('some-image.jpg'),
            ]
        );


        $this->postJson(
            route('image.upload'),
            [
                'image' => $this->fileFactory->image('some-image.jpg'),
            ]
        );

        $this->assertCount(2, $filesystem->allFiles('tmp'));

    }

    /**
     * @test
     */
    public function canFitPreviewImage(): void
    {
        $response = $this
            ->postJson(
                route('image.upload'),
                [
                    'image'   => $this->fileFactory->image('some-image.jpg'),
                    'preview' => [
                        'fit' => [
                            'width'  => 8,
                            'height' => 3
                        ]
                    ]
                ]
            );


        $path = $this->getPathFromUrl($response->json('url'));

        $this->assertImageSize($path, 8, 3);
    }

    private function assertImageSize(string $path, int $width, int $height): void
    {
        list($actualWIdth, $actualHeight) = getimagesize($path);

        $this->assertEquals($width, $actualWIdth);
        $this->assertEquals($height, $actualHeight);
    }

    /**
     * @test
     */
    public function canFitPreviewImageWithJsonConfig(): void
    {
        $response = $this
            ->postJson(
                route('image.upload'),
                [
                    'image'   => $this->fileFactory->image('some-image.jpg'),
                    'preview' => json_encode(['fit' => [
                        'width'  => 8,
                        'height' => 3
                    ]])
                ]
            );


        $path = $this->getPathFromUrl($response->json('url'));

        $this->assertImageSize($path, 8, 3);
    }

    /**
     * @test
     */
    public function canResizePreviewImage(): void
    {
        $response = $this
            ->postJson(
                route('image.upload'),
                [
                    'image'   => $this->fileFactory->image('some-image.jpg'),
                    'preview' => [
                        'resize' => [
                            'width' => 8,
                        ]
                    ]
                ]
            );


        $path = $this->getPathFromUrl($response->json('url'));

        $this->assertImageSize($path, 8, 8);
    }

    /**
     * @test
     */
    public function canResizePreviewImageWithJson(): void
    {
        $response = $this
            ->postJson(
                route('image.upload'),
                [
                    'image'   => $this->fileFactory->image('some-image.jpg'),
                    'preview' => json_encode([
                        'resize' => [
                            'width' => 8,
                        ]
                    ])
                ]
            );


        $path = $this->getPathFromUrl($response->json('url'));

        $this->assertImageSize($path, 8, 8);
    }

    protected function setUp(): void
    {
        parent::setUp();

        Storage::fake('public');
        $this->fileFactory = UploadedFile::fake();
        $this->fileSystem  = $this->app->make(Factory::class);

        config()->set('images.upload.route', 'foobar/images');

    }
}
