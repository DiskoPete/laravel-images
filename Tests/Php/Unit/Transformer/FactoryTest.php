<?php

namespace DiskoPete\LaravelImages\Tests\Php\Unit\Transformer;


use Illuminate\Foundation\Testing\RefreshDatabase;
use DiskoPete\LaravelImages\Models\Transformer;
use DiskoPete\LaravelImages\Models\Transformer\Factory;
use DiskoPete\LaravelImages\Tests\Php\Utils\Traits\CreatesTestModels;
use DiskoPete\LaravelImages\Tests\Php\TestCase;

class FactoryTest extends TestCase
{
    use RefreshDatabase;
    use CreatesTestModels;

    /**
     * @test
     */
    public function canMakeManipulator(): void
    {
        $factory     = $this->getFactory();
        $image       = $this->createImage();
        $manipulator = $factory->make($image);

        $this->assertInstanceOf(Transformer::class, $manipulator);
    }

    private function getFactory(): Factory
    {
        return $this->app->make(Factory::class);
    }
}