<?php

namespace DiskoPete\LaravelImages\Tests\Php\Unit\Traits;


use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use DiskoPete\LaravelImages\Models\Image;
use DiskoPete\LaravelImages\Tests\Php\Utils\Models\Plane;
use DiskoPete\LaravelImages\Tests\Php\Utils\Traits\CreatesTestModels;
use DiskoPete\LaravelImages\Traits\HasImages;
use DiskoPete\LaravelImages\Tests\Php\TestCase;

class HasImagesTest extends TestCase
{
    use RefreshDatabase;
    use CreatesTestModels;

    /**
     * @test
     */
    public function canGetSubjectsImages(): void
    {
        $plane = $this->createPlane();
        $image = $this->createImage([
            Image::COLUMN_SUBJECT_TYPE => get_class($plane),
            Image::COLUMN_SUBJECT_ID   => $plane->getKey()
        ]);


        $images = $plane->images;

        $this->assertUsesHasImages($plane);
        $this->assertInstanceOf(Collection::class, $images);
        $this->assertCount(1, $images);
        $this->assertTrue($images->contains($image));
    }

    /**
     * @test
     */
    public function imagesAreSorted(): void
    {
        $plane = $this->createPlane();
        $this->createImageForSubject($plane, [Image::COLUMN_POSITION => 20]);

        $secondImage = $this->createImageForSubject($plane, [Image::COLUMN_POSITION => 10]);

        $images = $plane->images;


        self::assertEquals(2, $secondImage->getKey());
        self::assertCount(2, $images);
        self::assertTrue($secondImage->is($images->first()));

    }

    private function createPlane(): Plane
    {
        return factory(Plane::class)->create();
    }

    private function assertUsesHasImages($model): void
    {
        $this->assertTrue(in_array(HasImages::class, class_uses($model)));
    }
}