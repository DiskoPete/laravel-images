<?php

namespace DiskoPete\LaravelImages\Tests\Php\Unit;


use Illuminate\Foundation\Testing\RefreshDatabase;
use DiskoPete\LaravelImages\Tests\Php\Utils\Traits\CreatesTestModels;
use DiskoPete\LaravelImages\Tests\Php\TestCase;

class ImageTest extends TestCase
{
    use RefreshDatabase;
    use CreatesTestModels;

    /**
     * @test
     */
    public function canGetAbsoluteFilePath(): void
    {
        $image = $this->createImage();
        $path = $image->getAbsolutePath();

        $this->assertTrue(file_exists($path));
    }

    /**
     * @test
     */
    public function canGetUrl(): void
    {
        $image = $this->createImage();

        $this->assertNotNull($image->getUrl());
    }
}