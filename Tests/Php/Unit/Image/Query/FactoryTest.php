<?php

namespace DiskoPete\LaravelImages\Tests\Php\Unit\Image\Query;


use DiskoPete\LaravelImages\Contracts\Image\Query\Factory as ImageQueryFactory;
use DiskoPete\LaravelImages\Models\Image\Query;
use DiskoPete\LaravelImages\Tests\Php\TestCase;

class FactoryTest extends TestCase
{
    /**
     * @test
     */
    public function canMakeQuery(): void
    {
        $factory = $this->makeFactory();

        $this->assertInstanceOf(Query::class, $factory->make());
    }

    private function makeFactory(): ImageQueryFactory
    {
        return app(ImageQueryFactory::class);
    }
}
