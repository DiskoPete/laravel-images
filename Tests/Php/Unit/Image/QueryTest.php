<?php

use DiskoPete\LaravelImages\Contracts\Image\Query\Factory as ImageQueryFactory;
use DiskoPete\LaravelImages\Models\Image;
use DiskoPete\LaravelImages\Tests\Php\Utils\Traits\CreatesTestModels;
use Illuminate\Foundation\Testing\RefreshDatabase;

class QueryTest extends \DiskoPete\LaravelImages\Tests\Php\TestCase
{

    use RefreshDatabase;
    use CreatesTestModels;

    /**
     * @test
     */
    public function canFilterBySubject(): void
    {
        $plane      = $this->createPlane();
        $helicopter = $this->createHelicopter();

        $this->createImage([
            Image::COLUMN_SUBJECT_TYPE => get_class($plane),
            Image::COLUMN_SUBJECT_ID   => $plane->getKey()
        ]);

        $helicopterImage = $this->createImage([
            Image::COLUMN_SUBJECT_TYPE => get_class($helicopter),
            Image::COLUMN_SUBJECT_ID   => $helicopter->getKey()
        ]);

        $query  = $this->makeQuery();
        $images = $query->addSubjectFilter($helicopter)->get();

        $this->assertCount(1, $images);
        $this->assertTrue($helicopterImage->is($images->first()));

    }

    private function makeQuery(): Image\Query
    {
        $factory = $this->getFactory();

        return $factory->make();
    }

    private function getFactory(): ImageQueryFactory
    {
        return app(ImageQueryFactory::class);
    }

    /**
     * @test
     */
    public function canFilterByType(): void
    {

        $this->createImage([
            Image::COLUMN_TYPE => 'foo'
        ]);

        $barImage = $this->createImage([
            Image::COLUMN_TYPE => 'bar'
        ]);

        $query  = $this->makeQuery();
        $images = $query->addTypeFilter('bar')->get();

        $this->assertCount(1, $images);
        $this->assertTrue($barImage->is($images->first()));

    }
}