<?php

namespace DiskoPete\LaravelImages\Tests\Php\Unit;


use DiskoPete\LaravelImages\Contracts\Image;
use DiskoPete\LaravelImages\Contracts\Transformer;
use DiskoPete\LaravelImages\Models\Transformer\Factory;
use DiskoPete\LaravelImages\Tests\Php\TestCase;
use DiskoPete\LaravelImages\Tests\Php\Utils\Traits\CreatesTestModels;
use Illuminate\Contracts\Container\Container;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Intervention\Image\ImageManager;

class TransformerTest extends TestCase
{
    use RefreshDatabase;
    use CreatesTestModels;

    /**
     * @test
     */
    public function canGetImageUrl(): void
    {
        $transformer = $this->createTransformer();

        $this->assertNotNull($transformer->getUrl());
    }

    private function createTransformer(?Image $image = null): Transformer
    {
        if (!$image) {
            $image = $this->createImage();
        }

        return $this->getTransformerFactory()->make(
            $image
        );
    }

    private function getTransformerFactory(): Factory
    {
        return $this->app->make(Factory::class);
    }

    /**
     * @test
     */
    public function canFitImage(): void
    {
        $image       = $this->createImage();
        $transformer = $this->createTransformer($image);

        list($origWidth, $origHeight) = getimagesize($image->getAbsolutePath());

        $this->assertEquals(10, $origWidth);
        $this->assertEquals(10, $origHeight);

        $transformer->fit(5, 5);
        $path = $this->getPathFromUrl($transformer->getUrl());

        list($newWidth, $newHeight) = getimagesize($path);

        $this->assertEquals(5, $newWidth);
        $this->assertEquals(5, $newHeight);

    }

    /**
     * @test
     */
    public function canGetNaturalDimensions(): void
    {
        $image       = $this->createImage();
        $transformer = $this->createTransformer($image);


        $transformer->fit(5, 5);
        $dimensions = $transformer->getNaturalDimensions();

        $this->assertEquals(5, $dimensions->getWidth());
        $this->assertEquals(5, $dimensions->getHeight());

    }

    /**
     * @test
     */
    public function doesNotUpsizeOnFitImage(): void
    {
        $image       = $this->createImage();
        $transformer = $this->createTransformer($image);

        list($origWidth, $origHeight) = getimagesize($image->getAbsolutePath());

        $this->assertEquals(10, $origWidth);
        $this->assertEquals(10, $origHeight);

        $transformer->fit(20, 10);
        $path = $this->getPathFromUrl($transformer->getUrl());

        list($newWidth, $newHeight) = getimagesize($path);

        $this->assertEquals(10, $newWidth);
        $this->assertEquals(5, $newHeight);

    }

    /**
     * @test
     */
    public function canResizeImage(): void
    {
        $image       = $this->createImage();
        $transformer = $this->createTransformer($image);

        list($origWidth, $origHeight) = getimagesize($image->getAbsolutePath());

        $this->assertEquals(10, $origWidth);
        $this->assertEquals(10, $origHeight);

        $transformer->resize(8);
        $path = $this->getPathFromUrl($transformer->getUrl());

        list($newWidth, $newHeight) = getimagesize($path);

        $this->assertEquals(8, $newWidth);
        $this->assertEquals(8, $newHeight);

    }

    /**
     * @test
     */
    public function cantUpsizeImageOnResize(): void
    {
        $image       = $this->createImage();
        $transformer = $this->createTransformer($image);

        list($origWidth, $origHeight) = getimagesize($image->getAbsolutePath());

        $this->assertEquals(10, $origWidth);
        $this->assertEquals(10, $origHeight);

        $transformer->resize(15);
        $path = $this->getPathFromUrl($transformer->getUrl());

        list($newWidth, $newHeight) = getimagesize($path);

        $this->assertEquals(10, $newWidth);
        $this->assertEquals(10, $newHeight);

    }

    /**
     * @test
     */
    public function returnsOriginalImageUrlWithoutTransformations(): void
    {
        $image       = $this->createImage();
        $transformer = $this->createTransformer($image);

        $this->assertEquals($image->getUrl(), $transformer->getUrl());

    }

    /**
     * @test
     */
    public function canCacheImages(): void
    {
        $image       = $this->createImage();
        $transformer = $this->createTransformer();

        /** @var Transformer $mock */
        $mock = $this
            ->getMockBuilder(get_class($transformer))
            ->setConstructorArgs([
                'image'        => $image,
                'imageManager' => $this->app->make(ImageManager::class),
                'container'    => $this->app->make(Container::class)
            ])
            ->setMethods(['applyTransformations'])
            ->getMock();

        $mock
            ->expects($this->once())
            ->method('applyTransformations');

        $mock->fit(5, 5);
        $mock->getUrl();
        $mock->getUrl();
    }
}