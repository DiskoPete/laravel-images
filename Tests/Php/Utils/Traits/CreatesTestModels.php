<?php

namespace DiskoPete\LaravelImages\Tests\Php\Utils\Traits;


use DiskoPete\LaravelImages\Models\Image;
use DiskoPete\LaravelImages\Tests\Php\Utils\Models\Helicopter;
use DiskoPete\LaravelImages\Tests\Php\Utils\Models\Plane;
use Illuminate\Database\Eloquent\Model;

trait CreatesTestModels
{
    protected function createImageForSubject(Model $subject, array $attributes = []): Image
    {
        return $this->createImage(
            array_merge(
                [
                    Image::COLUMN_SUBJECT_TYPE => get_class($subject),
                    Image::COLUMN_SUBJECT_ID   => $subject->getKey()
                ],
                $attributes
            )
        );
    }

    protected function createImage(array $attributes = []): Image
    {
        return factory(Image::class)->create($attributes);
    }

    private function createPlane(): Plane
    {
        return factory(Plane::class)->create();
    }

    private function createHelicopter(): Helicopter
    {
        return factory(Helicopter::class)->create();
    }
}
