<?php

namespace DiskoPete\LaravelImages\Tests\Php\Utils\Models;

use Illuminate\Database\Eloquent\Model;
use DiskoPete\LaravelImages\Traits\HasImages;

class Plane extends Model
{
    use HasImages;

    const TABLE_NAME = 'planes';

    protected $table = self::TABLE_NAME;
}
