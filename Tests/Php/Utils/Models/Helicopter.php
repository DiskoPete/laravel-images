<?php

namespace DiskoPete\LaravelImages\Tests\Php\Utils\Models;


use Illuminate\Database\Eloquent\Model;
use DiskoPete\LaravelImages\Traits\HasImages;

class Helicopter extends Model
{
    use HasImages;

    const TABLE_NAME = 'helicopters';

    protected $table = self::TABLE_NAME;
}
