<?php
/**
 * @var \Illuminate\Database\Eloquent\Factory $factory
 */

use DiskoPete\LaravelImages\Models\Image;
use Faker\Generator as Faker;
use DiskoPete\LaravelImages\Tests\Php\Utils\Models\Plane;

$factory->define(Image::class, function (Faker $faker) {

    $file = \Illuminate\Http\UploadedFile::fake()->image(uniqid() . '.jpg');
    $path = 'tmp';
    $file->store($path, ['disk' => 'public']);

    return [
        Image::COLUMN_PATH         => $file->hashName($path),
        Image::COLUMN_SUBJECT_TYPE => Plane::class,
        Image::COLUMN_SUBJECT_ID   => function () {
            return factory(Plane::class)->create()->getKey();
        }
    ];
});
